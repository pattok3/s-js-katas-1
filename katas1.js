function oneThroughTwenty() {
    
  console.log('1 - Listar de 1 a 20')
  for (let a = 1; a <= 20; a++){
    console.log(a)
  }
    
}
oneThroughTwenty()

function evensToTwenty() {
    
  console.log('2 - Pares de 1 a 20')

  for (let a = 2; a <= 20; a++){
    console.log(a++)
  }
  
}
evensToTwenty()

function oddsToTwenty() {
    
  console.log('3- Impares de 1 a 20')

  for (let a = 1; a <= 20; a++){
    console.log(a++)
  }

}
oddsToTwenty()

function multiplesOfFive() {
    
  console.log('4- Multiplos de 5 até 100')
  for (let a = 5; a <= 100; a++){
    if (a % 5 === 0){
        console.log(a)
    }
  }

}
multiplesOfFive()

function squareNumbers() {
    
 console.log('5 - Quadrados perfeitos até 100')
  
  let contador = 1
  let quadrado = 0

  while (quadrado < 100){
    
    quadrado = contador * contador

    console.log(quadrado)
    contador++
   }
    
}
squareNumbers()

function countingBackwards() {
    
  console.log('6 - 1 a 20 de trás pra frente')

  for (let a = 20; a > 0; a--){
    console.log(a)
  }

}
countingBackwards()

function evenNumbersBackwards() {
    
 console.log('7 - Pares de 1 a 20 de trás pra frente')

 for (let a = 20; a > 0; a--){
  console.log(a--)
  }

}
evenNumbersBackwards()

function oddNumbersBackwards() {
    
 console.log('8 - Impares de 1 a 20 de trás pra frente.')
 for (let a = 20; a > 0; a--){
  a = a - 1
  console.log(a)
  }

}
oddNumbersBackwards()

function multiplesOfFiveBackwards() {
    
 console.log('9 - Multiplos de 5 até 100 de trás para frente')
 for (let a = 100; a > 0; a--){
  if (a % 5 === 0){
      console.log(a)
    } 
  }

}
multiplesOfFiveBackwards()

function squareNumbersBackwards() {
    
  console.log('10 - Quadrados perfeitos até 10 de trás pra frente.')
  
  let contador = 10
  let quadrado = 100

  while (quadrado > 0){

    quadrado = contador * contador

    console.log(quadrado)
    contador--

   }
}
squareNumbersBackwards()